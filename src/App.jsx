import { useState } from 'react'
import blackOwlLogo from './assets/logo.jpeg'
import mapPin from './assets/map-pin.png'
import kg from './assets/kg.jpg'
import uc from './assets/uc.jpg'
import pik from './assets/pik1.jpg'
import viteLogo from '/vite.svg'
import './index.css'
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import Home from './Home'
import Events from './Event'
import Summary from './Summary'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/event" element={<Events />} />
          <Route path="/summary" element={<Summary />} />
        </Routes>
      </BrowserRouter>
      


    </>
  )
}

export default App
