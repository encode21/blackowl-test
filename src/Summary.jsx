import { useState } from 'react'
import blackOwlLogo from './assets/logo.jpeg'
import mapPin from './assets/map-pin.png'
import kg from './assets/kg.jpg'
import calendar from './assets/calendar-event.png'
import clock from './assets/clock.png'
import pik from './assets/pik1.jpg'
import user from './assets/user.png'
import mail from './assets/mail.png'
import phone from './assets/phone.png'
import notes from './assets/notes.png'
import table from './assets/table.png'
import pencil from './assets/pencil.png'
import layout from './assets/layout-sidebar.png'
import chevronRight from './assets/chevron-right.png'
import './index.css'
function Summary() {
  const [count, setCount] = useState(0)
  const [modalOpen, setModalOpen] = useState(false);
  const openModal = () => {
    setModalOpen(true)
  }
  return (
    <>
        {(modalOpen) && (
            <div id="static-modal" data-modal-backdrop="static" tabIndex="-1" aria-hidden="true" class="fixed top-0 left-0 right-0 z-50 w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full justify-center items-center flex">
            <div class="relative w-full max-w-2xl max-h-full">
                <div class="relative bg-white rounded-lg shadow dark:bg-gray-700">
                    <div class="flex items-start justify-between p-4 border-b rounded-t dark:border-gray-600">
                        <h3 class="text-xl font-semibold text-gray-900 dark:text-white">
                            Confirmation
                        </h3>
                        <button onClick={()=>setModalOpen(false)} type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-hide="static-modal">
                            <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                            </svg>
                            <span class="sr-only">Close modal</span>
                        </button>
                    </div>
                    <div class="py-6">
                        <p class="text-base justify-items-center text-center items-center leading-relaxed text-gray-500 dark:text-gray-400">
                            Are you sure the data you entered is correct?
                        </p>

                        <div className="w-full  flex-col justify-start items-start inline-flex">
                            <div className="self-stretch p-4 bg-white flex-col justify-start items-start gap-4 flex">
                                <div className="self-stretch text-neutral-700 text-base font-semibold font-['Poppins'] leading-tight">Detail Reservation</div>
                                <div className="self-stretch h-px border border-neutral-200"></div>
                                <div className="self-stretch justify-between items-start inline-flex">
                                    <div className="grow shrink basis-0 flex-col justify-start items-start gap-2 inline-flex">
                                        <div className="text-neutral-700 text-sm font-medium font-['Poppins'] leading-tight">Location</div>
                                        <div className="text-neutral-700 text-sm font-normal font-['Poppins'] leading-tight">BO-Pantai Indah Kapuk</div>
                                    </div>
                                    <div className="grow shrink basis-0 flex-col justify-start items-end gap-2 inline-flex">
                                        <div className="text-neutral-700 text-sm font-medium font-['Poppins'] leading-tight">Sunday</div>
                                        <div className="text-neutral-700 text-sm font-normal font-['Poppins'] leading-tight">01 Oct 2023 21:00 PM</div>
                                    </div>
                                </div>
                                <div className="self-stretch h-px border border-neutral-200"></div>
                                <div className="self-stretch flex-col justify-start items-start gap-2 flex">
                                    <div className="self-stretch text-neutral-700 text-sm font-medium font-['Poppins'] leading-tight">Guest Information</div>
                                    <div className="self-stretch flex-col justify-start items-start gap-2 flex">
                                        <div className="justify-start items-start gap-2 inline-flex">
                                            <div className="w-6 h-6 relative">
                                                <img src={user} alt="" />
                                            </div>
                                            <div className="text-neutral-700 text-base font-normal font-['Poppins'] leading-normal">Mrs. Ratih</div>
                                        </div>
                                        <div className="justify-start items-start gap-2 inline-flex">
                                            <div className="w-6 h-6 relative">
                                                <img className="" src={phone} />
                                            </div>
                                            <div className="text-neutral-700 text-base font-normal font-['Poppins'] leading-normal">+62 9435943850</div>
                                        </div>
                                        <div className="justify-start items-start gap-2 inline-flex">
                                            <div className="w-6 h-6 relative">
                                                <img className="" src={mail} />
                                            </div>
                                            <div className="text-neutral-700 text-base font-normal font-['Poppins'] leading-normal">ratih@gmail.com</div>
                                        </div>
                                        <div className="justify-start items-start gap-2 inline-flex">
                                            <div className="w-6 h-6 relative">
                                                <img src={notes} alt="" />
                                            </div>
                                            <div className="text-neutral-700 text-base font-normal font-['Poppins'] leading-normal">mohon sediakan selimut</div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div className="self-stretch flex-col justify-start items-start gap-2 flex">
                                    <div className="self-stretch text-neutral-700 text-sm font-medium font-['Poppins'] leading-tight">Table</div>
                                    <div className="self-stretch flex-col justify-start items-start gap-2 flex">
                                        <div className="justify-start items-start gap-2 inline-flex">
                                            <div className="w-6 h-6 relative">
                                                <img src={table} alt="" />
                                            </div>
                                            <div className="text-neutral-700 text-base font-normal font-['Poppins'] leading-normal">Floor</div>
                                        </div>
                                        <div className="justify-start items-start gap-2 inline-flex">
                                            <div className="w-6 h-6 relative">
                                                <img className="" src={layout} />
                                            </div>
                                            <div className="text-neutral-700 text-base font-normal font-['Poppins'] leading-normal">RSVP Session</div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                    <div class="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b dark:border-gray-600">
                        <button data-modal-hide="static-modal" style={{background:'#D6B87C'}} type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 w-2/4 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
                        <button onClick={()=>setModalOpen(false)} data-modal-hide="static-modal" type="button" class="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 w-2/4 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-600">Edit</button>
                    </div>
                </div>
            </div>
        </div>
        )}
      <nav class="bg-black border-gray-200 bg-gray-900">
        <div class=" flex flex-wrap items-center justify-between mx-auto ">
          <a href="/" class="flex items-center">
              <img src={blackOwlLogo} class="h-16 mr-3" alt="Flowbite Logo" />
              <span style={{
                // BLACK OWL
                color: '#D6B87C',
                fontFamily:'Roboto',
                fontSize: '18px',
                fontWeight: '400',
                lineHeight: '24px',
                wordWrap: 'break-word'
              }} class="self-center text-2xl text-white font-uppercase">BLACK OWL</span>
          </a>
          <button data-collapse-toggle="navbar-default" type="button" class="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="navbar-default" aria-expanded="false">
              <span class="sr-only">Open main menu</span>
              <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
                  <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 1h15M1 7h15M1 13h15"/>
              </svg>
          </button>
          <div class="hidden w-full md:block md:w-auto" id="navbar-default">
            <ul class="font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg md:flex-row md:space-x-8 md:mt-0 md:border-0 ">
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 text-white md:text-yellow-400" aria-current="page">Home</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">About</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">Services</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">Pricing</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">Contact</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div className="w-full px-4 my-4 flex-col justify-start items-start gap-4 inline-flex">
        <div className="self-stretch justify-start items-start gap-2.5 inline-flex">
            <div className="text-stone-400 text-base font-bold font-['Poppins']" style={{color:'#D6B87C'}}>Booking Summary</div>
        </div>
        <img className="self-stretch rounded-sm" src={pik} />
        <div className="self-stretch text-neutral-700 text-sm font-semibold font-['Poppins'] leading-tight">Time</div>
        <div className="self-stretch h-20 flex-col justify-start items-start gap-2 flex">
            <div className="justify-start items-start gap-2 inline-flex">
                <div className="w-6 h-6 relative">
                    <img src={mapPin} alt="" />
                </div>
                <div className="text-neutral-700 text-base font-normal font-['Poppins'] leading-normal">BO-Pantai Indah Kapuk</div>
            </div>
            <div className="justify-start items-start gap-2 inline-flex">
                <div className="w-6 h-6 relative">
                    <img className="" src={calendar} />
                </div>
                <div className="text-neutral-700 text-base font-normal font-['Poppins'] leading-normal">Sunday, 01 Oct 2023</div>
            </div>
            <div className="justify-start items-start gap-2 inline-flex">
                <div className="w-6 h-6 relative">
                    <img src={clock} alt="" />
                </div>
                <div className="text-neutral-700 text-base font-normal font-['Poppins'] leading-normal">21:00 pm</div>
            </div>
        </div>
    </div>
    <div className=" h-3.5 bg-orange-50" />
    <div className="mx-auto w-full px-2.5">
        <div className="w-full p-4 flex-col justify-start items-start gap-4 inline-flex">
            <div className="justify-start items-start gap-2.5 inline-flex">
                <div className="text-stone-400 text-base font-semibold font-['Poppins'] leading-normal" style={{color:'#D6B87C'}}>Additional Information</div>
            </div>
        </div>
        <div className="p-4">

        <form>
            <div>
                <div class="relative mb-6">
                    <div class="absolute inset-y-0 right-0 flex items-center pr-3.5 pointer-events-none">
                        <img src={user} alt="" />
                        </div>
                        <input type="text" id="floating_outlined" class="block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />
                        <label for="floating_outlined" class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1">Choose Title</label>
                    </div>
                </div>
                <div class="grid gap-6 mb-6 grid-cols-2">
                    
                    <div class="relative">
                        <div class="absolute inset-y-0 right-0 flex items-center pr-3.5 pointer-events-none">
                        <img src={user} alt="" />
                        </div>
                        <input type="text" id="fname" class="block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />
                        <label for="fname" class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1">First Name</label>
                    </div>
                    <div class="relative">
                        <div class="absolute inset-y-0 right-0 flex items-center pr-3.5 pointer-events-none">
                        <img src={user} alt="" />
                        </div>
                        <input type="text" id="lname" class="block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />
                        <label for="lname" class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1">Last Name</label>
                    </div> 
                    
                </div>
                <div class="relative mb-6">
                    <div class="absolute inset-y-0 right-0 flex items-center pr-3.5 pointer-events-none">
                    <img src={mail} alt="" />
                    </div>
                    <input type="text" id="email" class="block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />
                    <label for="email" class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1">Email Address</label>
                </div> 
                <div class="relative mb-6">
                    <div class="absolute inset-y-0 right-0 flex items-center pr-3.5 pointer-events-none">
                    <img src={phone} alt="" />
                    </div>
                    <input type="text" id="email" class="block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />
                    <label for="email" class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1">Phone Number</label>
                </div> 
                <div class="relative mb-6">
                    <div class="absolute inset-y-0 left-0 flex items-center pl-3.5 pointer-events-none">
                    <img src={pencil} alt="" />
                    </div>
                    <textarea type="text" id="email" class="block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />
                    <label for="email" class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1">Additional Note</label>
                </div>
                <div className="w-full text-center"><span className="text-zinc-600 text-sm font-normal font-['Poppins'] leading-tight">We Are Holding Your Reservation For </span><span className="text-red-600 text-sm font-semibold font-['Poppins'] leading-tight">14:23</span></div>
            </form>
        </div>
    </div>

    <div className="mx-auto mt-2 w-full">
        <div className="w-full shadow h-24 p-6 bg-white justify-start items-start gap-3 inline-flex">
            <button type="button" style={{background:'#fff',color:'#333'}} class="shadow w-50 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none w-2/4 items-center justify-center focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                Back
            </button>
            {/* <div className="grow shrink basis-0 h-10 px-3 py-2 bg-orange-300 rounded-lg shadow justify-center items-center gap-2 flex">
                <div className="text-center text-white text-sm font-semibold font-['Poppins'] leading-tight">Continue</div>
                <div className="w-6 h-6 relative" >
                    
                </div>
            </div> */}
            <button onClick={()=>{openModal()}} type="button" style={{background:'#D6B87C',color:'#fff'}} class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none  w-2/4 items-center justify-center focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                Continue
                <svg class="ml-2 w-3 h-3 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 8 14">
                    <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 13 5.7-5.326a.909.909 0 0 0 0-1.348L1 1"/>
                </svg>
            </button>
        </div>
    </div>


    


    </>
  )
}

export default Summary
