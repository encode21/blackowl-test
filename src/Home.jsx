import { useState } from 'react'
import blackOwlLogo from './assets/logo.jpeg'
import mapPin from './assets/map-pin.png'
import kg from './assets/kg.jpg'
import uc from './assets/uc.jpg'
import pik from './assets/pik1.jpg'
import viteLogo from '/vite.svg'
import './index.css'

function Home() {
  const [count, setCount] = useState(0)

  return (
    <>
      <nav class="bg-black border-gray-200 bg-gray-900">
        <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto ">
          <a href="/" class="flex items-center">
              <img src={blackOwlLogo} class="h-16 mr-3" alt="Flowbite Logo" />
              <span style={{
                // BLACK OWL
                color: '#D6B87C',
                fontFamily:'Roboto',
                fontSize: '18px',
                fontWeight: '400',
                lineHeight: '24px',
                wordWrap: 'break-word'
              }} class="self-center text-2xl text-white font-uppercase">BLACK OWL</span>
          </a>
          <button data-collapse-toggle="navbar-default" type="button" class="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="navbar-default" aria-expanded="false">
              <span class="sr-only">Open main menu</span>
              <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
                  <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 1h15M1 7h15M1 13h15"/>
              </svg>
          </button>
          <div class="hidden w-full md:block md:w-auto" id="navbar-default">
            <ul class="font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg md:flex-row md:space-x-8 md:mt-0 md:border-0 ">
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 text-white md:text-yellow-400" aria-current="page">Home</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">About</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">Services</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">Pricing</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">Contact</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div className="mx-auto justify-items-center items-center py-2 px-4">
        <div class="w-full h-20 justify-between items-start inline-flex">
          <div class="w-56"><span className="text-neutral-700 text-3xl font-extrabold leading-10">Choose Your </span><span className=" text-3xl font-extrabold leading-10" style={{color:'#D6B87C'}}>Place</span></div>
          <div class="w-12 h-12 relative">
            <div class="left[19.50px] top-[17.33px] absolute"><img src={mapPin} alt="" /></div>
          </div>
        </div>
      </div>
      <div className="w-full justify-items-center items-center py-2 px-4">
        <div className="text-neutral-700 text-xs font-normal leading-none">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna </div>
      </div>
      <div className="w-full  justify-items-center items-center py-2 px-4">
        <div class="bg-white border my-2 border-gray-200 rounded-sm shadow dark:bg-gray-800 dark:border-gray-700">
            <a href="/event">
                <img class="w-full rounded-t-sm" src={pik} alt="" />
            </a>
        </div>
        <div class="bg-white border my-2 border-gray-200 rounded-sm shadow dark:bg-gray-800 dark:border-gray-700">
            <a href="/event">
                <img class="w-full rounded-t-sm" src={kg} alt="" />
            </a>
        </div>
        <div class="bg-white border my-2 border-gray-200 rounded-sm shadow dark:bg-gray-800 dark:border-gray-700">
            <a href="/event">
                <img class="w-full rounded-t-sm" src={uc} alt="" />
            </a>
        </div>
      </div>
      <div className="text-center justify-items-center items-center py-2 px-4">
        <div><span className="text-neutral-700 text-xs font-normal leading-none">Read our</span><span className="text-blue-600 text-xs font-normal leading-none"> Terms & Condition, Privacy Policy</span></div>
      </div>


    </>
  )
}

export default Home
