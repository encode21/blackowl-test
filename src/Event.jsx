import { useState } from 'react'
import blackOwlLogo from './assets/logo.jpeg'
import blackOwlLogo2 from './assets/logo1.png'
import mapPin from './assets/map-pin.png'
import kg from './assets/kg.jpg'
import uc from './assets/uc.jpg'
import pik from './assets/pik1.jpg'
import calendar from './assets/calendar-event.png'
import chevronRight from './assets/chevron-right.png'
import './index.css'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'

function Events() {
  const [count, setCount] = useState(0)

  return (
    <>
      <nav class="bg-black border-gray-200 bg-gray-900">
        <div class="flex flex-wrap items-center justify-between mx-auto ">
          <a href="/" class="flex items-center">
              <img src={blackOwlLogo} class="h-16 mr-3" alt="Flowbite Logo" />
              <span style={{
                // BLACK OWL
                color: '#D6B87C',
                fontFamily:'Roboto',
                fontSize: '18px',
                fontWeight: '400',
                lineHeight: '24px',
                wordWrap: 'break-word'
              }} class="self-center text-2xl text-white font-uppercase">BLACK OWL</span>
          </a>
          <button data-collapse-toggle="navbar-default" type="button" class="inline-flex items-center p-2 w-10 h-10 justify-center text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="navbar-default" aria-expanded="false">
              <span class="sr-only">Open main menu</span>
              <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
                  <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 1h15M1 7h15M1 13h15"/>
              </svg>
          </button>
          <div class="hidden w-full md:block md:w-auto" id="navbar-default">
            <ul class="font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg md:flex-row md:space-x-8 md:mt-0 md:border-0 ">
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 text-white md:text-yellow-400" aria-current="page">Home</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">About</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">Services</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">Pricing</a>
              </li>
              <li>
                <a href="#" class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 text-white md:hover:text-yellow-400 hover:bg-gray-700 hover:text-white md:hover:bg-transparent">Contact</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="my-4 mx-auto w-full py-2 px-4">
        
        
        <form className='rounded-full'>   
            <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
            <div class="relative rounded-full">
                <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                    </svg>
                </div>
                <input type="search" style={{borderColor:'#D6B87C'}} id="default-search" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 bg-gray-50 focus:ring-blue-500 rounded-full focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search events..." required />
                <button type="submit" style={{background:"#D6B87C"}} class="text-white absolute right-2.5 bottom-2.5 bg-black-700 hover:bg-black-800 focus:ring-4 focus:outline-none rounded-full focus:ring-blue-300 font-medium  text-sm px-4 py-2 dark:bg-black-600 dark:hover:bg-black-700 dark:focus:ring-blue-800">Search</button>
            </div>
        </form>


      </div>
      <div className="my-4 mx-auto w-full py-2 px-4">

        <div class="w-full px-2 bg-none dark:bg-gray-800">
            <div class="flex items-center justify-between mb-4">
                <h5 class="text-lg font-light leading-none text-gray-900 dark:text-white">Upcoming Event</h5>
                <a href="#" class="text-sm font-medium text-blue-600 hover:underline dark:text-blue-500">
                    <img src={calendar} alt="" />
                </a>
            </div>

            <figure style={{borderColor: '#F1D59E', background: '#FFF9EB'}} class="mb-8 flex flex-col p-2 border border-brown-200 rounded-lg  dark:border-brown-700">
                <figcaption class="flex space-x-3">
                    <img class="" src={blackOwlLogo2} alt="profile picture" />
                    <div class="space-y-0.5 font-medium dark:text-white text-left">
                        <div>Black Owl Event</div>
                        <div class="text-sm text-gray-500 dark:text-gray-400">Apr 24, 2023</div>
                    </div>
                </figcaption>    
            </figure>

            <FullCalendar
                plugins={[ dayGridPlugin ]}
                initialView="dayGridMonth"
                
            />
        </div>
        

      </div>
    <div className=" h-3.5 bg-orange-50" />
    <div className="my-4 mx-auto w-full py-2 px-4">
        <div className="w-full h-72 p-4 flex-col justify-start items-start gap-4 inline-flex">
            <div className="justify-start items-start gap-2.5 inline-flex">
                <div className="text-stone-400 text-base font-semibold font-['Poppins'] leading-normal">Additional Information</div>
            </div>
            <div className="self-stretch h-56 flex-col justify-start items-start gap-3 flex">
                <div className="w-full h-14 flex-col justify-start items-start gap-0.5 flex">
                    <div class="w-full relative">
                        <select id="floating_outlined" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                            <option selected>00:00 - 00:00</option>
                            <option value="US">United States</option>
                            <option value="CA">Canada</option>
                            <option value="FR">France</option>
                            <option value="DE">Germany</option>
                        </select>
                        <label for="floating_outlined" class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1">Choose Time</label>
                    </div>
                </div>
                <div className="w-full h-14 flex-col justify-start items-start gap-0.5 flex">
                    <div class="relative w-full">
                        <input type="text" id="floating_outlined" class="block px-2.5 pb-2.5 pt-4 w-full text-sm text-gray-900 bg-transparent rounded-lg border-1 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " />
                        <label for="floating_outlined" class="absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-4 scale-75 top-2 z-10 origin-[0] bg-white dark:bg-gray-900 px-2 peer-focus:px-2 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:-translate-y-1/2 peer-placeholder-shown:top-1/2 peer-focus:top-2 peer-focus:scale-75 peer-focus:-translate-y-4 left-1">Choose Pax</label>
                    </div>
                </div>
                <div className="self-stretch h-20 px-4 flex-col justify-start items-start gap-3 flex">
                    <div className="text-neutral-950 text-xs font-semibold font-['Poppins'] leading-none">RSVP Area</div>
                    <div className="flex-col justify-start items-start gap-2 flex">
                        <div class="flex items-center mb-4">
                            <input id="default-radio-1" type="radio" value="" name="default-radio" class="w-4 h-4 text-yellow-600 bg-gray-100 border-gray-300 focus:ring-yellow-500 dark:focus:ring-yellow-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                            <label for="default-radio-1" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Floor</label>
                        </div>
                        <div class="flex items-center">
                            <input checked id="default-radio-2" type="radio" value="" name="default-radio" class="w-4 h-4 text-yellow-600 bg-gray-100 border-gray-300 focus:ring-yellow-500 dark:focus:ring-yellow-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600" />
                            <label for="default-radio-2" class="ml-2 text-sm font-medium text-gray-900 dark:text-gray-300">Room Karaoke</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div className=" h-3.5 bg-orange-50" />
    <div className="my-4 mx-auto w-full px-8">

        <div class="flex items-center">
            <input id="link-checkbox" type="checkbox" value="" class="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"/>
            <label for="link-checkbox" class="ml-2 text-sm font-light text-gray-900 dark:text-gray-300">I read and agree to the above <a href="#" class="text-blue-600 dark:text-blue-500 hover:underline">terms and conditions</a>.</label>
        </div>
    </div>

    <div className="mx-auto mt-2 w-full">
        <div className="w-full shadow h-24 p-6 bg-white justify-start items-start gap-3 inline-flex">
            <button type="button" style={{background:'#fff',color:'#333'}} class="shadow w-50 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none w-2/4 items-center justify-center focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                Back
            </button>
            {/* <div className="grow shrink basis-0 h-10 px-3 py-2 bg-orange-300 rounded-lg shadow justify-center items-center gap-2 flex">
                <div className="text-center text-white text-sm font-semibold font-['Poppins'] leading-tight">Continue</div>
                <div className="w-6 h-6 relative" >
                    
                </div>
            </div> */}
            <a href='/summary' type="button" style={{background:'#D6B87C',color:'#fff'}} class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none  w-2/4 items-center justify-center focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                Continue
                <svg class="ml-2 w-3 h-3 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 8 14">
                    <path stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 13 5.7-5.326a.909.909 0 0 0 0-1.348L1 1"/>
                </svg>
            </a>
        </div>
    </div>

    </>
  )
}

export default Events
